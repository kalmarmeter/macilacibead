#ifndef MACILACIMODEL_H
#define MACILACIMODEL_H

#include <QVector>
#include <QKeyEvent>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QDir>

#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "entity.h"

enum class ObjectType {
    Default,
    Open,
    Obstacle
};

enum class GameState {
    Playing,
    Lost,
    Won
};

struct Object {
    ObjectType type;
    bool seen;
    Object() : type(ObjectType::Open), seen(false) {}
    Object(ObjectType t, bool s) : type(t), seen(s) {}
};

class MaciLaciModel: public QObject {

    Q_OBJECT

using obj_vec_t = QVector<Object>;
using guard_vec_t = QVector<Guard*>;
using basket_vec_t = QVector<Basket*>;

private:
    int level_size;
    obj_vec_t level;

    int guard_num;
    guard_vec_t guards;

    int basket_num;
    basket_vec_t baskets;

    Player* player;

    int pos(int x, int y) const {return x + y*level_size;}

    int get_random() const;

    bool in(int x, int y) const {return (x>=0 && y>=0 && x<level_size && y<level_size);}

    GameState state = GameState::Playing;

    bool pause = false;

    int step_num;


public:

    MaciLaciModel();
    ~MaciLaciModel();

    int get_level_size() const {return level_size;}
    void set_level_size(int s) {if (s > 0) level_size = s;}

    const obj_vec_t& get_level_vec() const {return level;}
    const basket_vec_t& get_basket_vec() const {return baskets;}
    const guard_vec_t& get_guard_vec() const {return guards;}
    const Player* get_player() const {return player;}

    const Object get_level_loc(int,int);

    bool get_pause() const {return pause;}

    int get_step_num() const {return step_num;}

public:
    void generate_level(int,int,int);
    void load_level(QString);
    void move_player(int,int);
    void move_guards();
    void set_level_visibility();
    void check_game_state();
    void flip_pause();

signals:
    void update();
    void game_won();
    void game_lost();
    void file_error();

public:
    GameState get_game_state() const {return state;}
};

#endif // MACILACIMODEL_H
