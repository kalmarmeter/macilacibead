#include "entity.h"

Direction flip_dir(Direction d) {
    switch (d) {
    case Direction::Right:
        return Direction::Left;
        break;
    case Direction::Up:
        return Direction::Down;
        break;
    case Direction::Down:
        return Direction::Up;
        break;
    case Direction::Left:
        return Direction::Right;
        break;
    }
}
