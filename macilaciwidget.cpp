#include "macilaciwidget.h"

MaciLaciWidget::MaciLaciWidget(QWidget *parent) : QWidget(parent) {

    resize(640,640);
    block_size = 48;
    setWindowTitle("MaciLaci");

    lcd_steps = new QLCDNumber(this);
    lcd_baskets = new QLCDNumber(this);
    lcd_steps->setSegmentStyle(QLCDNumber::SegmentStyle::Flat);
    lcd_baskets->setSegmentStyle(QLCDNumber::SegmentStyle::Flat);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update_guards()));
    connect(&model, SIGNAL(game_won()), this, SLOT(game_won()));
    connect(&model, SIGNAL(game_lost()), this, SLOT(game_lost()));
    connect(&model, SIGNAL(update()), this, SLOT(update()));
    connect(&model, SIGNAL(file_error()), this, SLOT(file_error()));

    new_game();

    timer->start(1000);

}

MaciLaciWidget::~MaciLaciWidget() {}

void MaciLaciWidget::keyPressEvent(QKeyEvent* key_event) {
    switch(key_event->key()) {
    case Qt::Key_W:
        model.move_player(0,-1);
        break;
    case Qt::Key_A:
        model.move_player(-1,0);
        break;
    case Qt::Key_S:
        model.move_player(0,1);
        break;
    case Qt::Key_D:
        model.move_player(1,0);
        break;
    case Qt::Key_R:
        timer->stop();
        new_game();
        timer->start(1000);
        break;
    case Qt::Key_P:
        model.flip_pause();
        break;
    }

    lcd_baskets->display(model.get_basket_vec().size());
    lcd_steps->display(model.get_step_num());
}

void MaciLaciWidget::file_error() {
    QMessageBox::information(this, "Error", "Error while loading file!");

    new_game();

}

void MaciLaciWidget::game_lost() {
    timer->stop();

    QMessageBox::information(this, "Game Over", "You lost the game!");

    new_game();

}

void MaciLaciWidget::game_won() {
    timer->stop();

    QMessageBox::information(this, "Congratulations", "You won the game!");

    new_game();

}

void MaciLaciWidget::update_guards() {
    model.move_guards();
    model.set_level_visibility();
    model.check_game_state();
}

void MaciLaciWidget::new_game() {
    QMessageBox::StandardButton ret = QMessageBox::information(this, "Game setup", "Do you want to load an existing map?", QMessageBox::Yes, QMessageBox::No);

    if (ret == QMessageBox::Yes) {

        QString filename = QInputDialog::getText(this, "Map loading", "Enter the name of the level file");

        model.load_level(filename);

        level_size = model.get_level_size();
        resize(level_size * block_size, level_size * block_size + y_off);
        setFixedSize(level_size * block_size, level_size * block_size + y_off);

    } else {

        level_size = QInputDialog::getInt(this, "Map Generation", "Enter the desired size of the level", 10, 10, 40, 1);

        model.set_level_size(level_size);
        resize(level_size * block_size, level_size * block_size + y_off);
        setFixedSize(level_size * block_size, level_size * block_size + y_off);

        model.generate_level(6, level_size / 4, level_size / 3);
    }

    timer->start(1000);
}

void MaciLaciWidget::paintEvent(QPaintEvent*) {

    lcd_steps->setFixedSize(width()/3, y_off);
    lcd_baskets->setFixedSize(width()/3, y_off);
    lcd_steps->move(0,0);
    lcd_baskets->move(2*width()/3, 0);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QDir image_dir(QDir::currentPath() + "/images");
    QImage open_seen_img(image_dir.path() + "/gyep_seen48.bmp");
    QImage open_unseen_img(image_dir.path() + "/gyep48.bmp");
    QImage obst_img(image_dir.path() + "/fa48.bmp");
    QImage player_img(image_dir.path() + "/macilaci48.png");
    QImage guard_img(image_dir.path() + "/erdesz48.png");
    QImage basket_img(image_dir.path() + "/kosar48.png");

    QRect source(0,0,block_size,block_size);
    QRect target;

    painter.fillRect(0,0,width(),y_off,QColor(244,234,178));

    QBrush brush(QColor(96,65,24), Qt::BrushStyle::Dense1Pattern);
    painter.setBrush(brush);
    painter.drawRect(width() / 3, 0, width() / 3, y_off);

    int level_size = model.get_level_size();


    for (int x=0; x<level_size; ++x) {
        for (int y=0; y<level_size; ++y) {
            Object obj = model.get_level_loc(x, y);
            target = QRect(x*block_size, y*block_size + y_off, block_size, block_size);
            if (obj.type == ObjectType::Open) {
                if (obj.seen) {
                    painter.drawImage(target, open_seen_img, source);
                } else {
                    painter.drawImage(target, open_unseen_img, source);
                }
            } else if (obj.type == ObjectType::Obstacle) {
                painter.drawImage(target, obst_img, source);
            }
        }
    }

    const Player* player = model.get_player();

    target = QRect(player->get_x() * block_size, player->get_y() * block_size + y_off, block_size, block_size);
    painter.drawImage(target, player_img, source);

    const QVector<Guard*> guards = model.get_guard_vec();

    for (auto g : guards) {
        target = QRect(g->get_x() * block_size, g->get_y() * block_size + y_off, block_size, block_size);
        painter.drawImage(target, guard_img, source);
    }

    const QVector<Basket*> baskets = model.get_basket_vec();

    for (auto b : baskets) {
        target = QRect(b->get_x() * block_size, b->get_y() * block_size + y_off, block_size, block_size);
        painter.drawImage(target, basket_img, source);
    }

    if (model.get_pause()) {
        painter.fillRect(0,0,width(),height(), QColor(100,100,100,100));
    }

}
