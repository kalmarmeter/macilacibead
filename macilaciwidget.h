#ifndef MACILACIWIDGET_H
#define MACILACIWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPainter>
#include <QTimer>
#include <QInputDialog>
#include <QMessageBox>
#include <QLCDNumber>

#include <cmath>

#include "macilacimodel.h"


class MaciLaciWidget : public QWidget {
    Q_OBJECT

private:

    MaciLaciModel model;

    int block_size;
    int level_size;

    QTimer* timer;

    void new_game();

    QLCDNumber* lcd_steps;
    QLCDNumber* lcd_baskets;
    QHBoxLayout* points_layout;

    int y_off = 50;


public:
    MaciLaciWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

    ~MaciLaciWidget();

private slots:
    void update_guards();
    void game_lost();
    void game_won();
    void file_error();

};

#endif // MACILACIWIDGET_H
