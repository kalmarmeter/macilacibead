#ifndef ENTITY_H
#define ENTITY_H


enum class Direction {
    Right,
    Up,
    Left,
    Down
};

Direction flip_dir(Direction);

class Entity {
protected:
    int x, y;
    Entity() : x(0), y(0) {}
    Entity(int xx, int yy) : x(xx), y(yy) {}
public:
    int get_x() const {return x;}
    int get_y() const {return y;}
    virtual ~Entity() {}
};

class Guard : public Entity {
private:
    Direction dir;
public:
    Guard() : Entity(), dir(Direction::Right) {}
    Guard(int xx, int yy, Direction dd) : Entity(xx,yy), dir(dd) {}
    Direction get_dir() const {return dir;}
    void move_by(int xam, int yam) {x += xam; y += yam;}
    void set_dir(Direction dd) {dir = dd;}
};

class Player : public Entity {
public:
    Player() : Entity() {}
    Player(int xx, int yy) : Entity(xx,yy) {}
    void move_by(int xam, int yam) {x += xam; y += yam;}
};

class Basket : public Entity {
public:
    Basket() : Entity() {}
    Basket(int xx, int yy) : Entity(xx,yy) {}
};

#endif // ENTITY_H
