#include "macilacimodel.h"

MaciLaciModel::MaciLaciModel() {
    srand(time(0));
}

MaciLaciModel::~MaciLaciModel() {
    for(auto g : guards) delete g;
    for(auto b : baskets) delete b;
    delete player;
}

void MaciLaciModel::flip_pause() {
    pause = !pause;
    emit update();
}

void MaciLaciModel::load_level(QString filename) {
    QFile file(filename);


    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        emit file_error();
        return;
    }

    QTextStream text(&file);

    QString level_s;
    text.readLineInto(&level_s);
    level_size = level_s.toInt();

    qDebug() << level_size;

    level.clear();
    level.resize(level_size * level_size);
    for(auto g : guards) delete g;
    guards.clear();
    for(auto b : baskets) delete b;
    baskets.clear();

    for (int y=0; y<level_size; ++y) {
        QString level_line;
        text.readLineInto(&level_line);
        for (int x=0; x<level_size; ++x) {
            char ch;
            ch = level_line.at(x).toLatin1();
            switch (ch) {
            case '.':
                level[pos(x,y)].type = ObjectType::Open;
                break;
            case 'O':
                level[pos(x,y)].type = ObjectType::Obstacle;
                break;
            case 'b':
                level[pos(x,y)].type = ObjectType::Open;
                baskets.push_back(new Basket(x,y));
                break;
            case 'g':
                level[pos(x,y)].type = ObjectType::Open;
                guards.push_back(new Guard(x, y, static_cast<Direction>(rand() % 4)));
                break;
            default:
                level[pos(x,y)].type = ObjectType::Open;
                break;
            }
        }
    }

    player = new Player();

    step_num = 0;

    file.close();
}

void MaciLaciModel::generate_level(int tree_prob, int guard_num, int basket_num) {
    qDebug() << "Generation started\n";

    level.clear();
    level.resize(level_size * level_size);

    for (int x=0; x<level_size; ++x) {
        for (int y=0; y<level_size; ++y) {
            if (get_random() % tree_prob) {
                level[pos(x,y)].type = ObjectType::Open;
            } else {
                level[pos(x,y)].type = ObjectType::Obstacle;
            }
        }
    }

    qDebug() << "Level generated\n";

    for(auto g : guards) delete g;
    guards.clear();

    int x, y;

    for (int i=0; i<guard_num; ++i) {
        do
        {
            x = get_random() % level_size;
            y = get_random() % level_size;
        }
        while (level.at(pos(x,y)).type == ObjectType::Obstacle);

        int d = get_random() % 4;
        guards.push_back(new Guard(x,y,static_cast<Direction>(d)));
    }

    qDebug() << "Guards added\n";

    for(auto b : baskets) delete b;
    baskets.clear();

    for (int i=0; i<basket_num; ++i) {
        do
        {
            x = get_random() % level_size;
            y = get_random() % level_size;
        }
        while (level.at(pos(x,y)).type == ObjectType::Obstacle);

        baskets.push_back(new Basket(x,y));
    }

    qDebug() << "Baskets added\n";

    player = new Player();

    if (level.at(pos(0,0)).type == ObjectType::Obstacle) {
        level[pos(0,0)].type = ObjectType::Open;
    }

    step_num = 0;

    qDebug() << "Player added\n";
}

const Object MaciLaciModel::get_level_loc(int x, int y) {
    if (x >= 0 && y >= 0 && x < level_size && y < level_size) {
        return level.at(pos(x, y));
    }
    return Object();
}

int MaciLaciModel::get_random() const {
    return rand();
}

void MaciLaciModel::move_player(int xam, int yam) {
    if (!pause) {

        if (in(player->get_x() + xam, player->get_y() + yam) && level.at(pos(player->get_x() + xam, player->get_y() + yam)).type != ObjectType::Obstacle) {
            player->move_by(xam, yam);
            ++step_num;
        }
        auto it = std::find_if(baskets.begin(), baskets.end(), [&](Basket* b) {
                                                                return (b->get_x() == player->get_x() && b->get_y() == player->get_y());});
        if (it != baskets.end()) {
            baskets.erase(it);
        }

        check_game_state();
    }
}

void MaciLaciModel::move_guards() {
    if (!pause) {
        for (auto g : guards) {
            int x = g->get_x();
            int y = g->get_y();

            Direction dir = g->get_dir();
            int xam, yam;

            switch(dir) {
            case Direction::Up:
                xam = 0;
                yam = -1;
                break;
            case Direction::Left:
                xam = -1;
                yam = 0;
                break;
            case Direction::Down:
                xam = 0;
                yam = 1;
                break;
            case Direction::Right:
                xam = 1;
                yam = 0;
                break;
            }

            if(in(x + xam, y + yam)) {
                if (level.at(pos(x + xam, y + yam)).type != ObjectType::Obstacle) {
                    g->move_by(xam, yam);
                } else {
                    g->set_dir(flip_dir(dir));
                }
            } else {
                g->set_dir(flip_dir(dir));
            }
        }
    }
}

void MaciLaciModel::set_level_visibility() {
    for (int i=0; i<level_size*level_size; ++i) {
        level[i].seen = false;
    }

    for (auto g : guards) {
        int x = g->get_x();
        int y = g->get_y();
        for (int i=-1; i<=1; ++i) {
            for (int j=-1; j<=1; ++j) {
                if (in(x+i, y+j)) {
                    if (level.at(pos(x+i, y+j)).type != ObjectType::Obstacle) {
                        level[pos(x+i, y+j)].seen = true;
                    }
                }
            }
        }
    }
}

void MaciLaciModel::check_game_state() {
    if (level.at(pos(player->get_x(),player->get_y())).seen) {
        state = GameState::Lost;
        emit game_lost();
    } else if (!baskets.size()) {
        state = GameState::Won;
        emit game_won();
    } else {
        state = GameState::Playing;
    }

    emit update();
}
